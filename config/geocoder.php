<?php

return [
    /**
     * Your Google Maps Geocoding API key
     *
     * @see https://developers.google.com/maps/documentation/geocoding/get-api-key
     */
    'api_key' => env('GOOGLE_MAPS_GEOCODING_API_KEY', ''),

    /**
     * The language to which your responses should be translated into
     *
     * @see https://developers.google.com/maps/faq#languagesupport
     */
    'language' => env('GOOGLE_MAPS_GEOCODING_LOCALE', 'en'),

    /**
     * Location accuracies
     *
     * @see https://developers.google.com/maps/documentation/geocoding/intro#Results
     */
    'accuracies' => [
        0 => 'ROOFTOP',
        1 => 'RANGE_INTERPOLATED',
        2 => 'GEOMETRIC_CENTER',
        3 => 'APPROXIMATE'
    ]
];
