### About

Simple Laravel package for geocoding and reverse geocoding using Google's Geocoding API

### Installation

`composer require lkrhl/geocoder`

Publish the configuration file:

`php artisan vendor:publish --provider="Lkrhl\Geocoder\GeocoderServiceProvider" --tag="config"`

### Example

Using the `Geocoder` facade:

```php
// Convert address to coordinates
try {
    Geocoder::geocode("Champ de Mars, 2 Allée Adrienne Lecouvreur, Paris", "France", "75007");
} catch (GeocodingServiceConnectionError | GeocodingServiceError $ex) {
    // handle exceptions
}
/*
[
    "address" => "Champ de Mars, 2 Allée Adrienne Lecouvreur, 75007 Paris, France",
    "accuracy" => 0,
    "lat" => 48.8556475,
    "lng" => 2.2986304
]
*/

// Convert coordinates to address
try {
    Geocoder::reverseGeocode("41.8899435", "12.4943371");
} catch (GeocodingServiceConnectionError | GeocodingServiceError $ex) {
    // handle exceptions
}
/*
[
    "address" => "Piazza del Colosseo, 1, 00184 Roma RM, Italy",
    "accuracy" => 0,
    "lat" => 41.8899435,
    "lng" => 12.4943371
]
*/
```

or:

```php
$client = new \GuzzleHttp\Client();

$geocoder = new Geocoder($client);

// $geocoder->setApiKey($apiKey);
$geocoder->setApiKey(config('geocoder.api_key'));

// Set response language (optional)
$geocoder->setLanguage($language);

try {
    $geocoder->geocode($streetAddress, $country, $postalCode);
    $geocoder->reverseGeocode($latitude, $longitude);
} catch (GeocodingServiceConnectionError | GeocodingServiceError $ex) {
    // handle exceptions
}
```

### Misc.

The `accuracy` field corresponds to the `location_type` parameter:
```
0 => ROOFTOP (most accurate)
1 => RANGE_INTERPOLATED
2 => GEOMETRIC_CENTER
3 => APPROXIMATE
```
