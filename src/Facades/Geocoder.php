<?php

namespace Lkrhl\Geocoder\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class Geocoder
 *
 * @package Lkrhl\Geocoder\Facades
 * @method \Lkrhl\Geocoder\Geocoder setLanguage(string $language)
 * @method \Lkrhl\Geocoder\Geocoder setApiKey(string $apiKey)
 * @method \Lkrhl\Geocoder\Geocoder setAccuracies(array $accuracies)
 * @method array geocode(string $address, string $country, string $postalCode = '')
 * @method array reverseGeocode(string $latitude, string $longitude)
 * @throws \Lkrhl\Geocoder\Exceptions\GeocodingServiceConnectionError
 * @throws \Lkrhl\Geocoder\Exceptions\GeocodingServiceError
 */
class Geocoder extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'geocoder';
    }
}
