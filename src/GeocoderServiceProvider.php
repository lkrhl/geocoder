<?php

namespace Lkrhl\Geocoder;

use GuzzleHttp\Client;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class GeocoderServiceProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/geocoder.php', 'geocoder');

        $this->app->bind('geocoder', function ($app) {
            return (new Geocoder(new Client()))
                ->setApiKey(config('geocoder.api_key'))
                ->setLanguage(config('geocoder.language'))
                ->setAccuracies(config('geocoder.accuracies'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/geocoder.php' => config_path('geocoder.php')
        ], 'config');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['geocoder'];
    }
}
