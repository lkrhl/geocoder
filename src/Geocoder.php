<?php
declare(strict_types=1);

namespace Lkrhl\Geocoder;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Lkrhl\Geocoder\Exceptions\GeocodingServiceConnectionError;
use Lkrhl\Geocoder\Exceptions\GeocodingServiceError;

class Geocoder
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var string
     */
    protected $apiEndpoint = 'https://maps.googleapis.com/maps/api/geocode/json';

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $language = 'en';

    /**
     * @var array
     */
    protected $accuracies;

    /**
     * Geocoder constructor
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $address
     * @param string $country
     * @param string $postalCode
     * @return array
     * @throws GeocodingServiceConnectionError
     * @throws GeocodingServiceError
     */
    public function geocode(string $address, string $country, string $postalCode = ''): array
    {
        if (empty($address)) {
            return $this->emptyResponse();
        }

        $params = ['address' => "{$address}, {$country}"];

        if (!empty($postalCode)) {
            $params['components'] = "postal_code:{$postalCode}";
        }

        $data = $this->fetchData($this->makePayload($params));

        if (!empty($data->error_message)) {
            throw new GeocodingServiceError($data->error_message);
        }

        return empty($data->results) ? $this->emptyResponse() : $this->getResult($data);
    }

    /**
     * @param string $latitude
     * @param string $longitude
     * @return array
     * @throws GeocodingServiceConnectionError
     * @throws GeocodingServiceError
     */
    public function reverseGeocode(string $latitude, string $longitude): array
    {
        $data = $this->fetchData($this->makePayload(['latlng' => "{$latitude},{$longitude}"]));

        if (!empty($data->error_message)) {
            throw new GeocodingServiceError($data->error_message);
        }

        return empty($data->results) ? $this->emptyResponse() : $this->getResult($data);
    }

    /**
     * @param array $params
     * @return array
     */
    protected function makePayload(array $params): array
    {
        $parameters = array_merge($this->getDefaultParams(), $params);

        return ['query' => $parameters];
    }

    /**
     * @param array $payload
     * @return mixed
     * @throws GeocodingServiceConnectionError
     */
    protected function fetchData(array $payload)
    {
        try {
            $response = $this->client->get($this->apiEndpoint, $payload);

            if ($response->getStatusCode() !== 200) {
                throw new GeocodingServiceConnectionError($response->getReasonPhrase());
            }
        } catch (ClientException $ex) {
            throw new GeocodingServiceConnectionError($ex->getMessage(), $ex->getCode(), $ex);
        }

        return json_decode((string) $response->getBody());
    }

    /**
     * @param mixed $data
     * @return array
     */
    protected function getResult($data): array
    {
        return [
            'address' => $data->results[0]->formatted_address,
            'accuracy' => $this->getResultAccuracy($data->results[0]->geometry->location_type),
            'lat' => $data->results[0]->geometry->location->lat,
            'lng' => $data->results[0]->geometry->location->lng
        ];
    }

    /**
     * @param string $accuracy
     * @return int
     */
    protected function getResultAccuracy(string $accuracy): int
    {
        return array_search($accuracy, $this->accuracies, true);
    }

    /**
     * @return array
     */
    protected function getDefaultParams(): array
    {
        return [
            'key' => $this->apiKey,
            'language' => $this->language
        ];
    }

    /**
     * @return array
     */
    protected function emptyResponse(): array
    {
        return [
            'lat' => 0,
            'lng' => 0,
            'accuracy' => null,
            'address' => null
        ];
    }

    /**
     * @param string $apiKey
     * @return Geocoder
     */
    public function setApiKey(string $apiKey): self
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * @param string $language
     * @return Geocoder
     */
    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @param array $accuracies
     * @return Geocoder
     */
    public function setAccuracies(array $accuracies): self
    {
        $this->accuracies = $accuracies;

        return $this;
    }
}
